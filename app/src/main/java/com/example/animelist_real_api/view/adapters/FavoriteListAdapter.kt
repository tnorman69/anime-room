package com.example.animelist_real_api.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.animelist_real_api.databinding.AnimeListItemBinding
import com.example.animelist_real_api.model.apiModels.FavoritesPreview

class FavoriteListAdapter(
    private val navigate: (id: String) -> Unit,
    ) :
    RecyclerView
    .Adapter<FavoriteListAdapter
    .ViewHolder>() {

    private var currentFavorites: List<FavoritesPreview> = listOf()

    class ViewHolder(private val binding: AnimeListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun applyAnimeData(item: FavoritesPreview) = with(binding) {
            imageView.loadImage(item.animePosterImg)
            listItemName.text = item.animeTitle
            listItemName.textSize = 20F
            favoritesBtn.visibility = View.GONE
        }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteListAdapter.ViewHolder {
        val binding = AnimeListItemBinding
            .inflate(
                LayoutInflater
                    .from(parent.context), parent, false
            )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return currentFavorites.size
    }


    fun applyFavorites(data: List<FavoritesPreview>) {
        currentFavorites = data
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = currentFavorites[position]
        holder.itemView.setOnClickListener {
            navigate(item.animeId)
        }
        holder.applyAnimeData(item)
    }
}

private fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}