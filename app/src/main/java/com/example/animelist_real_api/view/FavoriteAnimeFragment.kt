package com.example.animelist_real_api.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.animelist_real_api.databinding.FragmentFavoriteAnimeBinding
import com.example.animelist_real_api.model.AnimeRepoImpl
import com.example.animelist_real_api.model.apiModels.FavoritesPreview
import com.example.animelist_real_api.util.Resource
import com.example.animelist_real_api.view.adapters.AnimeListAdapter
import com.example.animelist_real_api.view.adapters.FavoriteListAdapter
import com.example.animelist_real_api.viewmodel.AnimeListViewModel
import com.example.animelist_real_api.viewmodel.FavoriteAnimeViewModel
import com.example.animelist_real_api.viewmodel.factory.ViewModelFactoryAnimeList
import com.example.animelist_real_api.viewmodel.factory.ViewModelFactoryFavoriteAnime

class FavoriteAnimeFragment : Fragment() {
    private var _binding: FragmentFavoriteAnimeBinding? = null
    private val binding: FragmentFavoriteAnimeBinding get() = _binding!!

    val repoImp by lazy {
        AnimeRepoImpl(requireContext())
    }

    private val viewmodel by viewModels<FavoriteAnimeViewModel>() {
        ViewModelFactoryFavoriteAnime(repoImp)
    }

    private var favoriteList: List<FavoritesPreview> = listOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFavoriteAnimeBinding.inflate(inflater, container, false).also {
        _binding = it
        viewmodel.getFavoriteAnime()
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFavorites()
    }

    override fun onResume() {
        super.onResume()
        viewmodel.getFavoriteAnime()
    }

    private fun initFavorites() = with(binding){
        viewmodel.favoriteAnime.observe(viewLifecycleOwner){favorite ->
            when(favorite){
                is Resource.Error -> {}
                Resource.Loading<Unit>() -> {}
                is Resource.Success -> {
                    favoriteList = favorite.data
                    if (favoriteList.isEmpty()){
                        favoritesRV.visibility = View.GONE
                        favoriteAnimeLoadingTv.visibility = View.VISIBLE
                    }else{
                        favoritesRV.visibility = View.VISIBLE
                        favoriteAnimeLoadingTv.visibility = View.GONE
                        favoritesRV.layoutManager = LinearLayoutManager(context)
                        favoritesRV.adapter = FavoriteListAdapter(::navigate).apply {
                            applyFavorites(favoriteList)
                        }
                    }
                }
                else -> {}
            }
        }
    }

    private fun navigate(id: String) {
        val action = IntroAnimeListFragmentDirections.actionIntroAnimeListFragmentToAnimeDetailsFragment(id)
        findNavController().navigate(action)
    }

}