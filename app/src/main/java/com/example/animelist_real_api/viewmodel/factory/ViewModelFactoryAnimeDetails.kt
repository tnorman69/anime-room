package com.example.animelist_real_api.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.animelist_real_api.model.AnimeRepoImpl
import com.example.animelist_real_api.viewmodel.AnimeDetailsViewModel
import com.example.animelist_real_api.viewmodel.AnimeListViewModel

class ViewModelFactoryAnimeDetails(
    private val repo: AnimeRepoImpl
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AnimeDetailsViewModel(repo) as T
    }
}