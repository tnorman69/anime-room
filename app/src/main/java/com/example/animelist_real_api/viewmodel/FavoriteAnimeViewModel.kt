package com.example.animelist_real_api.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.animelist_real_api.model.AnimeRepoImpl
import com.example.animelist_real_api.model.apiModels.FavoritesPreview
import com.example.animelist_real_api.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FavoriteAnimeViewModel(private val repo: AnimeRepoImpl): ViewModel() {

    private var _favoriteAnime: MutableLiveData<Resource<List<FavoritesPreview>>> = MutableLiveData(
        Resource.Loading())
    val favoriteAnime: LiveData<Resource<List<FavoritesPreview>>> get() = _favoriteAnime

    fun getFavoriteAnime() = viewModelScope.launch(Dispatchers.Main) {
        val response = repo.getFavoriteAnimeFromDb()
        _favoriteAnime.value = response
    }
}